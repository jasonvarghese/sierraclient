require 'oauth2'
require 'rest_client'
require 'json'
require 'rsolr'


$host = ''
$token = ''


def getToken
  client = OAuth2::Client.new($token, 'nypltech', :site => $host, :token_url => 'iii/sierra-api/v1/token')
  token = client.client_credentials.get_token
	puts token
	tokenString = token.token
end
  

def getBibData(tokenString)
  response = RestClient.get $host+'/iii/sierra-api/v1/bibs', {'Authorization' => "Bearer #{tokenString}", :params=>{"fields"=>"default,fixedFields","deleted"=>"false"}}
	responseHash = JSON.parse(response)
	return responseHash
end

def getItemData(tokenString)
	response = RestClient.get $host+'/iii/sierra-api/v1/items/32288204', {'Authorization' => "Bearer #{tokenString}"}
	responseHash = JSON.parse(response)
	return responseHash
end


tokenString = getToken
responseHash = getBibData(tokenString)
puts responseHash

itemresponse = getItemData(tokenString)
puts itemresponse
responseHash['entries'].each{ |i|
puts "id =  #{i['id']}"
if(i['deletedDate'] == nil) then
  puts "title =  #{i['title']}"
  puts "--author =  #{i['author']}"
  puts "--publishDate = #{i['publishYear']}"
  puts "--format = #{i["materialType"]["value"]}"
  puts "--language = #{i["fixedFields"]["24"]["value"]}"
end

}




